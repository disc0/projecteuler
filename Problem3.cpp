/*
 * The prime factors of 13195 are 5, 7, 13 and 29.</p>
 * What is the largest prime factor of the number 600851475143 ?
*/

#include <iostream>
#include <cmath>
#include <vector>

using std::cout;
using std::cin;
using std::vector;

// Takes long n and returns a vector of the prime factors of n.
// Sorted smallest to largest
vector<long> primeFactors(long n)
{
    vector<long> factors;
    while (n % 2 == 0) // this goes until the number is no longer even
    {
        factors.push_back(2);
        n /= 2;
    }

    for (long i = 3; i <= sqrt(n); i += 2) // n can only be odd so every other possible factor can be skipped
    {
        while (n % i == 0)
        {
            factors.push_back(i);
            n /= i;
        }
    }

    if (n > 2) // input was a prime larger than 2
    {
        factors.push_back(n);
    }
    return factors;
}

int main()
{
    long toFactor;
    cout << "Enter a number to factor: ";
    cin >> toFactor;
    
    vector<long> factors = primeFactors(toFactor);
    
    for (long i: factors)
    {
        cout << i << " ";
    }
    cout << std::endl;
    return 0;
}

