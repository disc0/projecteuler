/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
*/

#include <iostream>
#include <string>

using namespace std;

int numMidpoint(int x)
{
    int len = to_string(x).length();
    
    if (len % 2 == 0)
    {
        return len / 2;
    }
    else
    {
        return len / 2 + 1;
    }
}

int reverseNum(int x)
{
    int reversedNum = 0;

    while (x != 0)
    {
        reversedNum = reversedNum*10 + (x % 10);
        x /= 10;
    }

    return reversedNum;
}

int main()
{
    // int num;
    // cin >> num;
    // cout << reverseNum(num);
    int prevNum = 0;
    for (int i = 999; i > 99; i--)
    {
        for (int j = 999; j > 99; j--)
        {
            int num = i * j;
            if (num == reverseNum(num))
            {
                if (num > prevNum)
                {
                    prevNum = num;
                    // cout << num << endl;
                }
            }
        }
    }
    cout << prevNum << endl;
    return 0;
}